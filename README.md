[![Latest Stable Version](https://poser.pugx.org/def-studio/laralog/v/stable)](https://packagist.org/packages/def-studio/laralog)
[![Total Downloads](https://poser.pugx.org/def-studio/laralog/downloads)](https://packagist.org/packages/def-studio/laralog)
[![License](https://poser.pugx.org/def-studio/laralog/license)](https://packagist.org/packages/def-studio/laralog)
[![composer.lock](https://poser.pugx.org/def-studio/laralog/composerlock)](https://packagist.org/packages/def-studio/laralog)


# LaraLog (Laravel 6.+)
Laravel's logging system helps a lot for storing data as well as while troubleshooting some hidden bugs. The data related to the exception automatically gets logged whenever something goes wrong.

Sometimes, we need more than just *stack trace* to debug the issue easily. The things like **request URL**, **request input data**, **session data**, etc. help us hunt down the exact cause quickly. That's what this *plug-and-play* Laravel package does for you :)


**Note**: For Laravel 5.5, you may use [Slack Error Notifier](https://github.com/freshbitsweb/slack-error-notifier) package.

## Requirements

* PHP 7.2+
* Laravel 6+

## Installation

1) Install the package by running this command in your terminal/cmd:
```
composer require def-studio/laralog
```

2) Publish migrations with the command:
```
php artisan vendor:publish --tag=laralog_migrations
php artisan migrate
```

3) Generate a new config with this command:
```
php artisan vendor:publish --tag=laralog_config
```

It has following configuration settings:
* (String) app => App name that will be written in each log entry
* (String) minimum_log_level => Minimum log level to be recorded (may be overwritten by connectors minimum_log_level)
* (String) log_php_errors => Add PHP error report to the logging system
* (Array) connectors => Where your log entries will be written
	* (Array) laravel_db => Writes log entries to the default laravel database, using Eloquent
		* (String) minimum_log_level => Minimum log level to be recorded (overwrites default minimum_log_level)
	* (Array) mysql => Writes log entries to a custom mysql database
		* (String) db_host => Database hostname
		* (String) db_name => Database name
		* (String) db_user => Database username
		* (String) db_password => Database password
		* (String) minimum_log_level => Minimum log level to be recorded (overwrites default minimum_log_level)
* (Array) save_lines => LaraLog may keep a reference to the lines of the file that generated the log entry
	* (Integer) before => How many lines before the log entry should be kept
	* (Integer) after => How many lines after the log entry should be kept
	* (String) min_level => Minimum log level for activating the file lines recording functionality
* (Array) context => file paths that are included in logging
* (Function) get_user_function => A custom function to get the username that generated the log entry
* (String) jwt_key => JWT key to encrypt API communications
* (String) jwt_iss => JWT issuer
 
4) (OPTIONAL) Set LaraLog as Laravel's default log channel:

First, open Laravel `config/logging.php` configuration file and add a new entry in `channels` array:
```
'laralog' => [
    'driver' => 'monolog',
    'handler' => \DefStudio\LaraLog\Handler::class,
    'level' => 'debug'
],
```
Then, in the same file, add a new channel entry in the `stack` channel:
```
'channels' => [
	'stack' => [
	    'driver' => 'stack',
	    'channels' => ['daily', 'laralog'],
	    'ignore_exceptions' => false,
	],
	...
```
leave `daily` channel if you want to keep laravel logging on `storage/log` along your LaraLog entries


## Authors

* [**Fabio Ivona [DeF Studio]**](https://defstudio.it)

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details