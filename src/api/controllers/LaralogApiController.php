<?php
	/**
	 * Powered by DeF Studio (report@defstudio.it)
	 * Date: 25/02/2019
	 * Time: 15:44
	 */
	
	namespace DefStudio\LaraLog\Controllers;
	
	use App\Http\Controllers\Controller;
	use DefStudio\LaraLog\LaraLog;
	use Firebase\JWT\JWT;
	use Illuminate\Http\Request;
	
	class LaralogApiController extends Controller{
		const STATUS_CODE_ERROR = 0;
		const STATUS_CODE_OK = 1;
		
		public function fetch(Request $request){
			
			
			$req = $request->get('request', null);
			
			if(empty($request)){
				return response()->json($this->error('No Valid Data'));
			}
			
			try{
				
				$token = $this->decode_jwt($req);
				$data = $token->data ?? null;
				
				if(empty($data)) return response()->json($this->error('Processing Error'));
				
				if(empty($data->api)) return response()->json($this->error('Processing error'));
				
				switch($data->api){
					case 'get_mysql_logs':
						return response()->json($this->get_logs($data));
						break;
					default:
						return response()->json($this->error('No valid API'));
						break;
				}
				
			} catch(\Exception $ex){
				return response()->json($this->error('Processing error'));
			}
		}
		
		private function get_logs($data){
			
			if(empty($data->from_id)) $data->from_id = -1;
			
			try{
				$logs = LaraLog::get_logs($data->from_id);
			} catch(\Exception $ex){
				
				return $this->error($ex->getMessage());
			}
			return $this->response(self::STATUS_CODE_OK, 'Ok', ['logs' => $logs]);
		}
		
		private function error($message){
			return $this->response(self::STATUS_CODE_ERROR, $message);
		}
		
		private function response($status_code, $message, $data = []){
			$encoded_data = $this->encode_jwt($data);
			
			return [
				'status'  => $status_code,
				'message' => $message,
				'jwt'     => $encoded_data,
			];
		}
		
		/**
		 * Encode data in a JWT token
		 * @param array $data
		 * @return null|string returns a JWT token or null if data is not valid
		 */
		private function encode_jwt($data = []){
			if(empty($data)){
				return null;
			}
			
			$token = [
				"iss"  => config('laralog.jwt_iss'),
				"data" => $data,
			];
			
			return JWT::encode($token, config('laralog.jwt_key'), 'HS256');
		}
		
		/**
		 * Decode a jwt token
		 * @param $data
		 * @return null|object
		 */
		private function decode_jwt($data){
			if(empty($data)){
				return null;
			}
			
			return JWT::decode($data, config('laralog.jwt_key'), ['HS256']);
		}
		
		
	}