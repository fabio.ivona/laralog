<?php
	/**
	 * Powered by DeF Studio (report@defstudio.it)
	 * Date: 25/02/2019
	 * Time: 16:48
	 */
	
	namespace DefStudio\LaraLog;
	
	use Illuminate\Database\Eloquent\Model;
	
	class Entry extends Model{
		
		const LEVEL_DEBUG = 'DEBUG';
		const LEVEL_INFO = 'INFO';
		const LEVEL_NOTICE = 'NOTICE';
		const LEVEL_WARNING = 'WARNING';
		const LEVEL_ERROR = 'ERROR';
		const LEVEL_CRITICAL = 'CRITICAL';
		const LEVEL_ALERT = 'ALERT';
		const LEVEL_EMERGENCY = 'EMERGENCY';
		
		protected $table = 'laralogs';
	}