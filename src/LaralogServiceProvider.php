<?php
	/**
	 * Powered by DeF Studio (report@defstudio.it)
	 * Date: 25/02/2019
	 * Time: 15:03
	 */
	
	namespace DefStudio\LaraLog;
	
	
	use Illuminate\Support\ServiceProvider;
	
	class LaralogServiceProvider extends ServiceProvider{
		public function boot(){
			
			$this->publishes([__DIR__.'/config/laralog.php' => config_path('laralog.php')], 'laralog_config');
			$this->publishes([__DIR__.'/database/migrations' => database_path('/migrations')], 'laralog_migrations');
			
			$this->loadRoutesFrom(__DIR__.'/routes/web.php');
			
			
			
		}
		
		public function register(){
			$this->mergeConfigFrom(__DIR__.'/config/laralog.php', 'laralog');
		}
		
		
	}