<?php
	/**
	 * Powered by DeF Studio (report@defstudio.it)
	 * Date: 27/02/2019
	 * Time: 09:37
	 */
	
	namespace DefStudio\LaraLog;
	
	
	use Monolog\Handler\AbstractProcessingHandler;
	
	class Handler extends AbstractProcessingHandler{
		
		
		protected function write(array $record): void{
			
			if(isset($record['context']['exception'])){
				$options = [
					'file' => $record['context']['exception']->getFile(),
					'line' => $record['context']['exception']->getLine()
				];
			}
			LaraLog::log($record['level_name'], $record['message'], $record['context'], $options??[]);
			
		}
	}