<?php
	/**
	 * Powered by DeF Studio (report@defstudio.it)
	 * Date: 25/02/2019
	 * Time: 16:22
	 */
	
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Database\Migrations\Migration;
	
	class LaralogCreateLaralogTable extends Migration
	{
		public function up()
		{
			Schema::create('laralogs', function(Blueprint $t)
			{
				$t->increments('id');
				$t->text('app', 128);
				$t->text('module', 128);
				$t->text('user', 64);
				$t->text('level', 16);
				$t->text('message', 256);
				$t->text('file', 512);
				$t->text('line', 10);
				$t->binary('file_lines');
				
				$t->timestamps();
			});
			
			DB::statement("ALTER TABLE laralogs ADD data MEDIUMBLOB");
		}
		
		public function down()
		{
			Schema::drop('laralogs');
		}
	}