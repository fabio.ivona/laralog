<?php
	namespace DefStudio\Logger;
	
	return [
		'app' => config('app.name'),
		'connectors' => [
			'laravel_db' => []
		],
		'minimum_log_level' => 'INFO',
		'log_php_errors' => true,
		'save_lines' => [
			'before' => 10,
			'after' => 10,
			'min_level' => 'ERROR'
		],
		'context' => [
			"/",
		],
		'get_user_function' => function(){
			$user = \Auth::user();
			if($user){
				return $user->email;
			}else{
				return "guest";
			}
			
		},
		'jwt_key' => 'XXX_THIS_IS_A_DEFAULT_JEY_PLEASE_CHANGE_IT_!!!',
		'jwt_iss' => 'Logger',
	];