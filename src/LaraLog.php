<?php
	/**
	 * Powered by DeF Studio (report@defstudio.it)
	 * Date: 25/02/2019
	 * Time: 16:52
	 */
	
	namespace DefStudio\LaraLog;
	
	
	class LaraLog{
		
		
		private static function level_map($level){
			switch($level){
				case Entry::LEVEL_DEBUG:
					return 100;
				case Entry::LEVEL_INFO:
					return 200;
				case Entry::LEVEL_NOTICE:
					return 250;
				case Entry::LEVEL_WARNING:
					return 300;
				case Entry::LEVEL_ERROR:
					return 400;
				case Entry::LEVEL_CRITICAL:
					return 500;
				case Entry::LEVEL_ALERT:
					return 550;
				case Entry::LEVEL_EMERGENCY:
					return 600;
				default:
					return 0;
			}
		}
		
		/**
		 * @param $level
		 * @param $message
		 * @param $data
		 * @param $options
		 */
		public static function log($level, $message, $data='', $options=[]){
			if(self::level_map($level) >= self::level_map(config('laralog.minimum_log_level'))){
				
				$stack = debug_backtrace(false);
				$backtrace = '';
				foreach($stack as $stack_entry){
					if(
						!preg_match('/def-studio\/laralog/', $stack_entry['file']) &&
						!preg_match('/monolog\/monolog/', $stack_entry['file']) &&
						!preg_match('/Illuminate\/Log/', $stack_entry['file']) &&
						!preg_match('/Illuminate\/Support/', $stack_entry['file'])
					){
						$backtrace = $stack_entry;
						break;
					}
					
					
				}
				
				//sets default options
				if(empty($options['user'])) $options['user'] = is_callable(config('laralog.get_user_function')) ? call_user_func(config('laralog.get_user_function')) : '';
				if(empty($options['module'])) $options['module'] = '';
				if(empty($options['app'])) $options['app'] = config('laralog.app');
				if(empty($options['file'])) $options['file'] = (empty($backtrace["file"]) ? '' : $backtrace["file"]);
				if(empty($options['line'])) $options['line'] = (empty($backtrace["line"]) ? '' : $backtrace["line"]);
				
				
				$data = self::convert_to_array($data);
				$data = json_encode($data);
				
				//check context
				$context_ok = true;
				if(config('laralog.context')){
					$context_ok = false;
					foreach(config('laralog.context') as $context){
						if(strpos($options['file'], $context) !== false){
							$context_ok = true;
						}
					}
				}
				
				if($context_ok){
					
					//salva le righe del file
					$lines = [];
					//TODO: impostare livello minimo di memorizzazione delle rige
					if(config('laralog.save_lines') && self::level_map($level) >= self::level_map(config('laralog.save_lines.min_level'))){
						$lines = self::get_file($options['file'], $options['line'], config('laralog.save_lines.before'), config('laralog.save_lines.after'));
					}
					
					if(config('laralog.connectors.mysql', false)){
						self::log_db($level, $message, $data, $lines, $options);
					}
					
					if(config('laralog.connectors.laravel_db')!==null){
						self::log_laravel_db($level, $message, $data, $lines, $options);
					}
				}
			}
		}
		
		
		private static function convert_to_array($object){
			$result = self::object_to_array_recusive($object);
			return $result;
		}
		
		private static function object_to_array_recusive($object, $nesting=0){
			
			$nesting++;
			
			if($nesting>10){
				return ["Max nesting level reached"];
			}
			
			$result = [];
			
			if(is_object($object)){
				$array = (array) $object;
				$class = get_class($object);
				foreach($array as $key => $value){
					$key_split = explode("\0", $key);
					$key = end($key_split);
					$result[$class][$key] = self::object_to_array_recusive($value, $nesting);
				}
				return $result;
			} else if(is_array($object)){
				$array = $object;
				foreach($array as $key => $value){
					$result[$key] = self::object_to_array_recusive($value, $nesting);
				}
				return $result;
			} else{
				return $object;
			}
			
			
		}
		
		private static function get_file($file, $line, $before, $after){
			if(empty($file)) return [];
			if(empty($line)) $line = 0;
			if(empty($before)) $before = 0;
			if(empty($after)) $after = PHP_INT_MAX;
			
			$from_line = $line - $before;
			$to_line = $line + $after;
			
			if($from_line < 0) $from_line = 0;
			
			if($to_line < $from_line) return [];
			
			if(!file_exists($file)) return [];
			
			$lines = file($file);
			
			if($to_line >= count($lines)){
				$to_line = count($lines) - 1;
			}
			
			$slice = [];
			
			for($i = $from_line; $i <= $to_line; $i++){
				$slice[$i + 1] = highlight_string($lines[$i], true);
			}
			
			return $slice;
		}
		
		
		/**
		 * @param $level
		 * @param $message
		 * @param $data
		 * @param $options
		 * @throws Exception
		 */
		private static function log_db($level, $message, $data, $lines, $options){
			
			dd('not implemented');
		}
		
		private static function log_laravel_db($level, $message, $data, $lines, $options){
			
			$entry = new Entry();
			$entry->app = $options['app'];
			$entry->module = $options['module'];
			$entry->user = $options['user'];
			$entry->file = $options['file'];
			$entry->line = $options['line'];
			$entry->file_lines = json_encode($lines);
			$entry->level = $level;
			$entry->message = $message;
			$entry->data = $data;
			
			$entry->save();
		}
		
		
		//<editor-fold desc="PUBLIC METHODS">
		
		/**
		 * Writes a debug entry
		 * @param $message
		 * @param $data
		 * @param array $options ([user, module, app, file, line])
		 */
		public static function debug($message, $data = '', $options = []){
			self::log(Entry::LEVEL_DEBUG, $message, $data, $options);
		}
		
		/**
		 * Writes an info entry
		 * @param $message
		 * @param $data
		 * @param array $options ([user, module, app, file, line])
		 */
		public static function info($message, $data = '', $options = []){
			self::log(Entry::LEVEL_INFO, $message, $data, $options);
		}
		
		/**
		 * Writes an notice entry
		 * @param $message
		 * @param $data
		 * @param array $options ([user, module, app, file, line])
		 */
		public static function notice($message, $data = '', $options = []){
			self::log(Entry::LEVEL_NOTICE, $message, $data, $options);
		}
		
		/**
		 * Writes a warning entry
		 * @param $message
		 * @param $data
		 * @param array $options ([user, module, app, file, line])
		 */
		public static function warning($message, $data = '', $options = []){
			self::log(Entry::LEVEL_WARNING, $message, $data, $options);
		}
		
		/**
		 * Writes a error entry
		 * @param $message
		 * @param $data
		 * @param array $options ([user, module, app, file, line])
		 */
		public static function error($message, $data = '', $options = []){
			self::log(Entry::LEVEL_ERROR, $message, $data, $options);
		}
		
		/**
		 * Writes a critical entry
		 * @param $message
		 * @param $data
		 * @param array $options ([user, module, app, file, line])
		 */
		public static function critical($message, $data = '', $options = []){
			self::log(Entry::LEVEL_CRITICAL, $message, $data, $options);
		}
		
		/**
		 * Writes an emergency entry
		 * @param $message
		 * @param $data
		 * @param array $options ([user, module, app, file, line])
		 */
		public static function emergency($message, $data = '', $options = []){
			self::log(Entry::LEVEL_EMERGENCY, $message, $data, $options);
		}
		
		/**
		 * Writes an alert entry
		 * @param $message
		 * @param $data
		 * @param array $options ([user, module, app, file, line])
		 */
		public static function alert($message, $data = '', $options = []){
			self::log(Entry::LEVEL_ALERT, $message, $data, $options);
		}
		
		/**
		 * Retrieves mysql log entries
		 * @param int $from_id starting entry ID (default -1)
		 * @return array
		 */
		public static function get_logs($from_id = -1){
			
			$entries = Entry::where('id', '>=', $from_id)->get();
			
			
			return $entries->toArray();
			
		}
		
		
		//</editor-fold>
	}